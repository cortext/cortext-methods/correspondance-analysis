FROM debian:stretch-slim
LABEL net.cortext.name="cortext-methods/correspondance-analysis"
LABEL net.cortext.authors="CorTexT Team <https://www.cortext.net>"
LABEL net.cortext.vcs-url="https://gitlab.com/cortext/cortext-methods/correspondance-analysis"

RUN echo "deb http://archive.debian.org/debian stretch main contrib non-free" > /etc/apt/sources.list
RUN echo "deb http://archive.debian.org/debian-security stretch/updates main" >> /etc/apt/sources.list
RUN DEBIAN_FRONTEND=noninteractive apt-get -y update && apt-get -y upgrade
RUN DEBIAN_FRONTEND=noninteractive apt-get -y --force-yes install \
    git \
    python \
    python-pip \
    python-yaml \
    python-pathlib \
    python-jsonpickle \
    python-sqlite \
    python-simplejson \
    r-cran-boot \
    r-cran-car \
    r-cran-class \
    r-cran-cluster \
    r-cran-codetools \
    r-cran-ellipse \
    r-cran-foreign \
    r-cran-kernsmooth \
    r-cran-lattice \
    r-cran-littler \
    r-cran-lme4 \
    r-cran-mass \
    r-cran-matrix \
    r-cran-matrixmodels \
    r-cran-mgcv \
    r-cran-minqa \
    r-cran-nlme \
    r-cran-nloptr \
    r-cran-nnet \
    r-cran-pbkrtest \
    r-cran-pkgkitten \
    r-cran-quantreg \
    r-cran-rcpp \
    r-cran-rcppeigen \
    r-cran-rpart \
    r-cran-sparsem \
    r-cran-spatial \
    r-cran-survival
RUN python2 -m pip install --upgrade pip
RUN pip install rpy2==2.6.0
RUN R -e 'install.packages("https://cran.r-project.org/src/contrib/Archive/flashClust/flashClust_1.01.tar.gz")'
RUN R -e 'install.packages("https://cran.r-project.org/src/contrib/Archive/leaps/leaps_2.9.tar.gz")'
RUN R -e 'install.packages("https://cran.r-project.org/src/contrib/Archive/scatterplot3d/scatterplot3d_0.3-36.tar.gz")'
RUN R -e 'install.packages("https://cran.r-project.org/src/contrib/Archive/FactoMineR/FactoMineR_1.31.3.tar.gz")'

# Install the latest `main` version of cortextlib and librarypy
RUN python2 -m pip install --ignore-requires-python --no-deps \
    git+https://gitlab.com/cortext/cortext-methods/cortextlib.git
RUN python2 -m pip install \
    git+https://gitlab.com/cortext/cortext-methods/librarypy.git
