# CorTexT Correspondance Analysis

CorTexT Correspondance Analysis is a method in
[CorTexT Manager](https://docs.cortext.net/) that provides original
visualizations of factor analysis as computed using FactoMiner library in R.

For more usage details, see the method's
[user documentation](https://docs.cortext.net/correspondance-analysis/).

## Local deployment

Initially, it is essential to perform the following steps on your local machine:

- Inside the vagrant run the commands below
  - `make purge-cortext-manager deploy-cortext-manager`
- Build the docker image inside vagrant in `cortext-methods-transition/correspondance-analysis`
  - `docker build -t cortext-methods/correspondance-analysis .`

## License

Copyright (C) 2024 CorTexT

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
