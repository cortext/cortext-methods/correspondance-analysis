# correspondance-analysis tests

## sample: `tsara-wos-df-experts-et-wos-cnrs.zip`

- sample available on [cortext-env](https://gitlab.com/cortext/operations/cortext-env) repository
- path: `cortext-env/tests/samples/tsara-wos-df-experts-et-wos-cnrs.zip`
- size: 725.95 kB

## test case 1

1. upload the corpus file `tsara-wos-df-experts-et-wos-cnrs.zip`
2. run Data Parsing
   - Corpus Format: isi
3. run Correspondance Analysis
   - id Fields: Address
   - Fields: ISIAF
4. expected output: directory `CA_results` with the content:
   - `burt_ISIAF-supp--contrib.csv` (467 B)
   - `burt_ISIAF-supp--coord.csv` (482 B)
   - `burt_ISIAF-supp--eig.csv` (243 B)
   - `burt_ISIAF-supp-.html` (1.98 kB)
   - `burt_ISIAF-supp-2.html` (1.97 kB)
   - `burt_ISIAF-supp-3.html` (1.97 kB)
   - `burt_ISIAF-supp-color.html` (2.01 kB)
