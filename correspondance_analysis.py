#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cortextlib.legacy import containerize

import os, sys
reload(sys) 
sys.setdefaultencoding("utf-8")
from sqlite3 import *
import types
from librarypy.path import *
from librarypy import fonctions
import math
import logging


#############################################################################
#############################################################################
#############################LOADING PARAMETERS##############################
#############################################################################
#############################################################################

print 'user_parameters',user_parameters
parameters_user=fonctions.load_parameters(user_parameters)
print parameters_user
timing = parameters_user.get('periods','ISIpubdate')
if timing == 'Custom Periods':
	time_table = 'ISIpubdate_custom'
else:
	time_table = 'ISIpubdate'
corpus_file = parameters_user.get('corpus_file','')
result_path=parameters_user.get('result_path','')
result_path0=result_path[:]
fonctions.progress(result_path0,0)

ca_results_path=os.path.join(result_path,'CA_results')
try:
	os.mkdir(ca_results_path)
except:
	pass
result_path=ca_results_path
top=int(parameters_user.get('top',10))
table_source_base=parameters_user.get('table_source_base','')
tables_source=parameters_user.get('tables_source','')
tables_source_supp=parameters_user.get('tables_source_supp','')
print 'tables_source',tables_source
try:
	nb_period=int(parameters_user.get('nb_period',1))
except:
	try:
		nb_period=list(parameters_user.get('nb_period',1))
	except:
		nb_period_range = parameters_user.get('nb_period','')
		eval('nb_period = ' + nb_period_range)
time_cut_type=parameters_user.get('time_cut_type','homogeneous')
print 'time_cut_type',time_cut_type
corpus_type=parameters_user.get('corpus_type','isi')
try:
	print 'here we are'
	from librarypy import descriptor
	data=descriptor.get_descriptor(corpus_file)
	#print 'data',data
	corpus_type=data['corpus_type']
except:
	corpus_type='other'

bdd_name = corpus_file

filename_v = fonctions.get_data(bdd_name,'ISIpubdate',limit_id=1)
try:
	filename=filename_v.values()[0][0]['file']
except:
	filename='f'

dico_champs = {}
if corpus_type=='isi':
	dico_champs['Keywords'] = 'ISIkeyword'
	dico_champs['Cited Author'] = 'ISICRAuthor'
	dico_champs['Cited Journal'] = 'ISICRJourn'
	dico_champs['Cited References'] = 'ISICitedRef'
	dico_champs['References'] = 'ISIRef'
	dico_champs['Research Institutions'] = 'ISIC1Inst'
	dico_champs['Subject Category'] = 'ISISC'
	dico_champs['Countries'] = 'ISIC1Country'
	dico_champs['Journal'] = 'ISIJOURNAL'
	dico_champs['Journal (long)'] = 'ISISO'
	dico_champs['Cities'] = 'ISIC1City'
	dico_champs['Author'] = 'ISIAUTHOR'
	dico_champs['Terms'] = 'ISIterms'
	dico_champs['Cited References'] = 'ISICitedRef'
	dico_champs['cp'] = 'cp'
	dico_champs['Country'] = 'Country'
	dico_champs['Year'] = 'ISIpubdate'
	dico_champs['Periods'] = 'ISIpubdate_custom'
	dico_champs['WOS Category'] = 'ISIWC'
	dico_champs['Publication Type'] = 'ISIDT'

dico_champs['Terms']='ISIterms'
dico_champs['Periods'] = 'ISIpubdate_custom'
if corpus_type=='xmlpubmed' or corpus_type=='isi' or corpus_type=='ris' :
	dico_champs['Year'] = 'ISIpubdate'
else:
	dico_champs['Time Steps'] = 'ISIpubdate'

tables_source=map(lambda x: dico_champs.get(x,x),tables_source)
print 'table sources:',' & '.join(tables_source)

table_source_base=dico_champs.get(table_source_base,table_source_base)
if len(tables_source_supp)>0:
	tables_source_supp=map(lambda x: dico_champs.get(x,x),tables_source_supp)
else:
	tables_source_supp=[]
	
print 'table sources_supp:',' & '.join(tables_source_supp)


print 'corpus_file',corpus_file
print 'result_path',result_path
print 'time_cut_type',time_cut_type
print 'nb_period',nb_period
print 'corpus_type',corpus_type
print 'tables_source',tables_source
print 'tables_source_supp',tables_source_supp
print 'top',top


import unicodedata
def strip_accents(s):
	try:
		return ''.join(c for c in unicodedata.normalize('NFD', s)
                  if unicodedata.category(c) != 'Mn')#.replace('/','')
	except:
		return ''.join(c for c in s)#.replace('/','')	

#############################################################################
#############################################################################
#################################FONCTIONS################################
#############################################################################
#############################################################################
def convert2dict(list_couple):
	dict={}
	try:
		for couple in list_couple:
			dict.setdefault(couple[0],[]).append(couple[1])
	except:
		pass
	return dict 



#################################
###logging user parameters#######
#################################
print 'creating logging file',os.path.join(result_path0,'.user.log')
logging.basicConfig(filename=os.path.join(result_path0,'.user.log'), filemode='a', level=logging.DEBUG,format='%(asctime)s %(levelname)s : %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
logger = logging.getLogger()
logging.info('Correspondance Analysis script started')
logger.handlers[0].flush()
yamlfile = 'correspondance_analysis.yaml'
if 'cointet' in os.getcwd():
	yamlfile = '/Users/jpcointet/Desktop/cortext/manager/scripts/correspondance_analysis/'+yamlfile
parameterslog=fonctions.getuserparameters(yamlfile,parameters_user)
logger = logging.getLogger()
logging.info(parameterslog)
logger.handlers[0].flush()
fonctions.progress(result_path0,2)
##################################
### end logging user parameters###
##################################

#############################################################################
#############################################################################
##########################PROCESSING DATA####################################
#############################################################################
#############################################################################
conn,curs=fonctions.connexion(corpus_file)
data={}
size_distribution={}
tables_multiple_ranks={}
enumnb=0
if len(table_source_base)>0:
	base_index = {}
	results = conn.execute(" SELECT id,rank,parserank,data FROM " +table_source_base + ' group by data' ).fetchall()#base_index permet d'accéder à l'index de l'ensemble des "individus" du panel
	print " SELECT id,rank,parserank,data FROM " +table_source_base + ' group by data'
	for res in results:
		(id,rank,parserank,datares)=res
		base_index.setdefault(id,[]).append(rank)
		enumnb+=1
print "len(base_index)",enumnb
unique_value_per_entry=	parameters_user.get('unique_value_per_entry',False)
#print 'base_index',base_index
for iii,tablesource in enumerate(tables_source+tables_source_supp):
	multiple_category=False
	size_distribution[tablesource]={}
	
	logging.info('Retrieving data from ' + tablesource)
	prog=2+int(50*iii/(len(tables_source+tables_source_supp)))
	fonctions.progress(result_path0,prog)
	
	results = conn.execute(" SELECT id,rank,parserank,data FROM " +tablesource ).fetchall()
	print "SELECT id,rank,parserank,data FROM " +tablesource
	print 'len(results)',len(results)
	for res in results:
		(id,rank,parserank,datares)=res
		datares = str(datares).replace(' ','-').replace('\t','-').replace("'",'').replace('"','')
		if not id in data:
			data[id]={}
		if unique_value_per_entry:
			if not (rank,datares) in data[id].get(tablesource,[]):
				data[id].setdefault(tablesource,[]).append((rank,datares))
			# else:
			# 	print 'duplicate entry',id,rank,datares
		else:
			data[id].setdefault(tablesource,[]).append((rank,datares))
		#print data[id][tablesource]
		if len(convert2dict(data[id][tablesource]).keys())>1:
			multiple_category = True
		size_distribution[tablesource][datares]=size_distribution[tablesource].get(datares,0)+1
	tables_multiple_ranks[tablesource]=multiple_category
authorized_distribution={}
#print "len(data)",len(data)
tables_partition={}
for tablesource in tables_source+tables_source_supp:
	N=1
	clause=True
	for id,notice in data.iteritems():
		if N==1:
			ranks=base_index.get(id,[])
			#print 'tablesource',tablesource,tables_multiple_ranks[tablesource],
			#print 'notice[tablesource]',notice[tablesource]
			data_dict=convert2dict(notice.get(tablesource,[()]))
			if tables_multiple_ranks[tablesource]:
				for rank in ranks:
					N=len(data_dict.get(rank,[]))
					if N!=1:
						clause=False
			else:
				try:
					N = len(data_dict.values()[0])
				except:
					N=0
				if N!=1:
					clause=False

	print 'tablesource N ',tablesource,':',N,clause
	if clause:
		tables_partition[tablesource]=True
	else:
		tables_partition[tablesource]=False
	tables_partition[tablesource]=False


def morethan(dico,N):
	for x in dico.keys():
		if dico[x]<N:
			#print ('remove',x,dico[x],N)
			del(dico[x])
	return dico
	
	
nb_var=0
nb_col_active=0
print 'tables_multiple_ranks',tables_multiple_ranks
print 'tables_partition',tables_partition
for tablesource in tables_source:
	authorized_distribution[tablesource]=fonctions.extractNtop(size_distribution[tablesource],N=top,type='dict')
	if int(parameters_user.get('minimum_number',0))>0:
		#print ('\t',tablesource,morethan(fonctions.extractNtop(size_distribution[tablesource],N=top,type='dict'),int(parameters_user.get('minimum_number',0))))
		authorized_distribution[tablesource]=morethan(fonctions.extractNtop(size_distribution[tablesource],N=top,type='dict'),int(parameters_user.get('minimum_number',0)))
	
	#remove empty categories
	dico={}
	for x, y in authorized_distribution[tablesource].iteritems():
		if len(x)>0:
			dico[x]=y
	authorized_distribution[tablesource]=dico
	
	
	if not tables_partition[tablesource]: 
		nb_col_active+=len(authorized_distribution[tablesource].keys())
		nb_var+=2*len(authorized_distribution[tablesource].keys())
	else:
		nb_col_active+=1
		nb_var+=len(authorized_distribution[tablesource].keys())
	
print 'nb_col_active',nb_col_active
nb_col_passive=0

	
print 'tables_partition',tables_partition
for tablesource in tables_source_supp:
	authorized_distribution[tablesource]=fonctions.extractNtop(size_distribution[tablesource],N=top,type='dict')
	#print (tablesource,fonctions.extractNtop(size_distribution[tablesource],N=top,type='dict'))
	if int(parameters_user.get('minimum_number',0))>0:
		#print ('\t',tablesource,morethan(fonctions.extractNtop(size_distribution[tablesource],N=top,type='dict'),int(parameters_user.get('minimum_number',0))))
		authorized_distribution[tablesource]=morethan(fonctions.extractNtop(size_distribution[tablesource],N=top,type='dict'),int(parameters_user.get('minimum_number',0)))
	
	#remove empty categories
	dico={}
	for x, y in authorized_distribution[tablesource].iteritems():
		if len(x)>0:
			dico[x]=y
	authorized_distribution[tablesource]=dico
	
		
	nb_var+=2*len(authorized_distribution[tablesource].keys())
	if not tables_partition[tablesource]: 
		nb_col_passive+=len(authorized_distribution[tablesource].keys())
	else:
		nb_col_passive+=1
#print 'data',data
#print 'size_distribution',size_distribution



logging.info('Burt contingency file ready')
fonctions.progress(result_path0,60)

burt_file_name = 'burt_'+'_'.join(tables_source)+'-supp-' + '_'.join(tables_source_supp)+'.txt'
burt_file_name = os.path.join(result_path,burt_file_name[:150][:-4]+'.txt')
print 'burt_file_name',burt_file_name
import codecs
burt_file=codecs.open(burt_file_name,'w','utf8')
#burt_file.write('\t'.join(tables_source)+'\n')
MCA_filename = os.path.join(result_path,burt_file_name.replace('.txt','.pdf'))
MCA_filename_ellipse = os.path.join(result_path,burt_file_name.replace('.txt','-ellipse.pdf'))
MCA_filename_coodinate = os.path.join(result_path,burt_file_name.replace('.txt','-coord.csv'))
MCA_filename_contrib = os.path.join(result_path,burt_file_name.replace('.txt','-contrib.csv'))
MCA_filename_supp_coodinate = os.path.join(result_path,burt_file_name.replace('.txt','supp-coord.csv'))

MCA_filename_eig = os.path.join(result_path,burt_file_name.replace('.txt','-eig.csv'))
html_MCA_filename= os.path.join(result_path,burt_file_name.replace('.txt','.html'))
	
def rename(tablesource):
	if 'ISIterms' in tablesource:
		name_tablesource=tablesource[8:].upper()[:2]+'_'
	else:
		if 'ISI' in tablesource[:3]:
			name_tablesource=tablesource[3:].upper()[:2]+'_'
		else:
			name_tablesource=tablesource.upper()[:2050]+'_'
	return 	name_tablesource.replace('\t',' ').replace(' ','').replace("'",'')

def get_first_strings(notice,tablesource,allowed_ranks):
	#print "allowed_ranks",allowed_ranks
	data = notice.get(tablesource,None)
	data_flats = []
	name_tablesource=rename(tablesource)
	#if tables_multiple_ranks[tablesource]:#rank
	if 1:
		#print 'allowed_ranks',allowed_ranks
		if len(allowed_ranks)>0:
			#print 'data',data
			data_dict = convert2dict(data)
			#print 'data_dict',data_dict
			for rank in allowed_ranks:
				data_flat=[]#['']
				if  (tables_multiple_ranks[tablesource] and rank in data_dict) or not tables_multiple_ranks[tablesource]:
					#print 'ici'
					if tables_multiple_ranks[tablesource]:
						values = data_dict[rank]
					else:
						
						try:
							values = data_dict.values()[0]
						except:
							values=['No_'+tablesource]
							#print 'values',values
					#print 'data_dict[rank]',rank,data_dict[rank]
					for x in authorized_distribution[tablesource]:
						#print 'x',x
						if x in values:
							#print 'ben oui'
							
							data_flat.append(name_tablesource+x)
						else:
							#print 'ben non'
							if not tables_partition[tablesource]:
								data_flat.append(name_tablesource+'Not.'+x)
							#added vite fait bien fait...
							else:
								data_flat.append(name_tablesource+'Not.'+x)
				else:
					if not tables_partition[tablesource]:
						for x in authorized_distribution[tablesource]:
							data_flat.append(name_tablesource+'Not.'+x)
				#print len(data_flat)
				data_flats.append(data_flat)
		# else:
		# 	for x in authorized_distribution[tablesource]:
		# 		if x in data:
		# 			data_flat.append(name_tablesource+x.replace('\t',' '))
		# 		else:
		# 			data_flat.append(name_tablesource+'Not.'+x.replace('\t',' '))
	# else:
	# 	#print 'allowed_ranks',allowed_ranks
	# 	if len(allowed_ranks)>0:
	# 		#print 'data',data
	# 		data_dict = convert2dict(data)
	# 		for rank in allowed_ranks:
	# 			pass
	# 			
	# 	if data[0] in authorized_distribution[tablesource]:
	# 		data_flat.append(name_tablesource+str(data[0]).replace('\t',' '))
	# 	else:
	# 		data_flat.append('')
	return data_flats
		# 
		# 	return 
		# if notice.get(tablesource,[''])[0] in authorized_distribution[tablesource]:
		# 	return str(notice.get(tablesource,'')[0])
		# else:
		# 	return tablesource+'_empty'
#print 'authorized_distribution',authorized_distribution	
for tab in authorized_distribution:
	i=0
	for x in authorized_distribution[tab]:
		i+=1
		if i<100:
			print x
			
for id,notice in data.iteritems():
	if 1:
		#print 'id',id,notice
		rows={}
		ranks=base_index.get(id,[])
		for tablesource in tables_source+tables_source_supp:
			#print "tablesource,get_first_strings(notice,tablesource,ranks) ", tablesource
			#print '###:',get_first_strings(notice,tablesource,ranks) 
			rows[tablesource]=get_first_strings(notice,tablesource,ranks)
			#print 'row',row
		#print 'ranks',ranks
		#print 'rows',rows
		for rank in range(len(ranks)):
			row=[]
			#print len(ranks)
			#print ranks
			for tablesource in tables_source+tables_source_supp:

				row = row + rows[tablesource][rank]
				#if tables_multiple_ranks[tablesource]:
					#print 'rank',rank
					#print "rows[tablesource]",tablesource,rows[tablesource]
					#row.append(rows[tablesource][rank])

				# else:
				# 	#print 'rows[tablesource]',rows[tablesource]
				# 	#row.append(rows[tablesource][0])
				# 	row = row + rows[tablesource][0]
			#print 'row',row
			#print str('\t'.join(row)+'\n').encode('utf8')
			burt_file.write(strip_accents('\t'.join(row)+'\n').encode('utf8'))
		
	#burt_file.write('\t'.join(map(lambda x: get_first_strings(notice,x).replace('\t',' ')[:300],tables_source))+'\t'+'\t'.join(map(lambda x: get_first_strings(notice,x).replace('\t',' ')[:300],tables_source_supp))+'\n')
	
burt_file.close()
#burt_file_name="http://factominer.free.fr/book/gmo.csv"


dict_nodes={}
for groupe,nodes in authorized_distribution.iteritems():
	group_short = rename(groupe)
	for node,taille in nodes.iteritems():
		dict_nodes[group_short+strip_accents(node)]={}
		dict_nodes[group_short+strip_accents(node)]['name']=node
		dict_nodes[group_short+strip_accents(node)]['groupe']=groupe
		dict_nodes[group_short+strip_accents(node)]['size']=taille
		#print group_short+strip_accents(node)
		if 'NE_' in group_short+strip_accents(node):
			dict_nodes[group_short+strip_accents(node)]['size']=0.1
#	 {'ISItermsscaleofdiscourse': {u'groupes ethniques/sociaux': 1580, u'proche': 19334, u'individu': 3541, u'quartier': 2961, u'gang': 3988, u'm\xe9tropole': 1449, u'probl\xe8mes publics': 505, u'Etat/soci\xe9t\xe9/citoyens': 1496, u'institutions': 6803}, 'ISItermspersonallink': {u'connaissance': 2009, u'lien familial': 662, u'personal link author/victim': 5, u'adresse victime': 2606, u'lien amical': 705, u'souvenirs communs': 445}}
#print 'dict_nodes',dict_nodes




from rpy2 import *
from rpy2.robjects import r


logging.info('MCA analysis unin FactoMiner library')
fonctions.progress(result_path0,70)


chaine = """
x=2
print(x)
library(FactoMineR)
gmo <- read.table(\""""
chaine = chaine+burt_file_name
chaine =chaine+"""",header=FALSE,sep="\\t")
res.mca <- MCA(gmo,quali.sup=bonus,graph = FALSE)


write.csv(res.mca$var$coord, file = "facto-coordinates.csv")
write.csv(res.mca$var$contrib, file = "facto-contrib.csv")
write.csv(res.mca$eig, file = "facto-eig.csv")
write.csv(res.mca$quali.sup$coord, file = "facto-coordinates_supp.csv")


"""
#plot.MCA(res.mca,invisible=c("ind"),cex=0.5,autoLab = "yes")
#dev.off()
#pdf("myplot.pdf")

#print 'chaine',chaine.replace('myplot.pdf',MCA_filename).replace(',quali.sup=bonus',',quali.sup=c('+ str(len(tables_source)+1)+':'+str(len(tables_source)+len(tables_source_supp)) +')')
#plot.MCA(res,invisible=c("ind"),cex=0.5,autoLab = "yes")

if nb_var==0:
	logging.debug('Error: You should choose at least one variable to analyze')
sizefactor = 2. / math.sqrt(nb_var)
print 'nb_var',nb_var
print 'sizefactor',sizefactor



def arrondi(nb):
	nb=float(nb)
	return "%.2f" %nb
	 
if len(tables_source_supp)==0:
	print chaine.replace('myplot.pdf',MCA_filename).replace('ellipse.pdf',MCA_filename_ellipse).replace('facto-eig.csv',MCA_filename_eig).replace('facto-contrib.csv',MCA_filename_contrib).replace('facto-coordinates.csv',MCA_filename_coodinate).replace(',quali.sup=bonus','').replace(',cex=0.5',',cex='+str(sizefactor))
	r(chaine.replace('myplot.pdf',MCA_filename).replace('ellipse.pdf',MCA_filename_ellipse).replace('facto-eig.csv',MCA_filename_eig).replace('facto-contrib.csv',MCA_filename_contrib).replace('facto-coordinates.csv',MCA_filename_coodinate).replace(',quali.sup=bonus','').replace(',cex=0.5',',cex='+str(sizefactor)).replace('write.csv(res.mca$quali.sup$coord, file = "facto-coordinates_supp.csv")',''))
else:	
	print chaine.replace('myplot.pdf',MCA_filename).replace('ellipse.pdf',MCA_filename_ellipse).replace('facto-eig.csv',MCA_filename_eig).replace('facto-contrib.csv',MCA_filename_contrib).replace('facto-coordinates.csv',MCA_filename_coodinate).replace(',quali.sup=bonus',',quali.sup=c('+ str(nb_col_active+1)+':'+str(nb_col_passive+nb_col_active) +')').replace(',cex=0.5',',cex='+str(sizefactor)).replace("facto-coordinates_supp.csv",MCA_filename_supp_coodinate)
	r(chaine.replace('myplot.pdf',MCA_filename).replace('ellipse.pdf',MCA_filename_ellipse).replace('facto-eig.csv',MCA_filename_eig).replace('facto-contrib.csv',MCA_filename_contrib).replace('facto-coordinates.csv',MCA_filename_coodinate).replace(',quali.sup=bonus',',quali.sup=c('+ str(nb_col_active+1)+':'+str(nb_col_passive+nb_col_active) +')').replace(',cex=0.5',',cex='+str(sizefactor)).replace("facto-coordinates_supp.csv",MCA_filename_supp_coodinate))





import csv,json
eigenvalues = csv.DictReader(file(MCA_filename_eig),delimiter=(','))
weight={}
for eigen in eigenvalues:
	weight[eigen['']]=float(arrondi(float(eigen['percentage of variance'])))

print 'dict_nodes',dict_nodes
print 'weight',weight
print 'MCA_filename_coodinate',MCA_filename_coodinate
coordinates = csv.DictReader(file(MCA_filename_coodinate),delimiter=(','))
print 'coordinates',coordinates
minz=999
maxz=-999
for coordinate in coordinates:
	print 'coordinate',coordinate
	try:
		dict_nodes[coordinate['']]['x']=float(arrondi(float(coordinate['Dim 1'])))
		dict_nodes[coordinate['']]['y']=float(arrondi(float(coordinate['Dim 2'])))
		dict_nodes[coordinate['']]['z']=float(arrondi(float(coordinate['Dim 3'])))
		minz = min(minz,float(arrondi(float(coordinate['Dim 3']))))
		maxz = max(maxz,float(arrondi(float(coordinate['Dim 3']))))
	except:
		pass

if len(tables_source_supp)>0:
	coordinates_supp = csv.DictReader(file(MCA_filename_supp_coodinate),delimiter=(','))
	for coordinate in coordinates_supp:
		try:
			dict_nodes[coordinate['']]['x']=float(arrondi(float(coordinate['Dim 1'])))
			dict_nodes[coordinate['']]['y']=float(arrondi(float(coordinate['Dim 2'])))
			dict_nodes[coordinate['']]['z']=float(arrondi(float(coordinate['Dim 3'])))
			minz = min(minz,float(arrondi(float(coordinate['Dim 3']))))
			maxz = max(maxz,float(arrondi(float(coordinate['Dim 3']))))
		except:
			pass

	

print 'dict_nodes',dict_nodes

legende = ['name','x','y','groupe','size']
sizes = []
final = [legende]
for x,y in dict_nodes.iteritems():
	try:#NB0_0 removed
		print 'y',y
		finalrow = []
		for leg in legende:
			#print 'leg',leg
			finalrow.append(y[leg])
			sizes.append(y['size'])
		final.append(finalrow)
	except:
		pass

logging.info('Generating visualizations')
fonctions.progress(result_path0,90)

size_mean = reduce(lambda x, y: x + y, sizes) / float(len(sizes))
#print "json.dumps(dict_nodes.values())",json.dumps(final)

html_part1="""
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>
      Google Visualization API Sample
    </title>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
    </script>
    <script type="text/javascript">
      function drawVisualization() {
          // Create and populate the data table.
          var data = google.visualization.arrayToDataTable(

"""

html_part2="""
      );
            var options = {
            title: 'MCA analysis',
            hAxis: {title: 'Dim 1'},
            vAxis: {title: 'Dim 2'},
            bubble: {textStyle: {fontSize: 11}}
          };
      
          // Create and draw the visualization.
          var chart = new google.visualization.BubbleChart(
              document.getElementById('visualization'));
          chart.draw(data, options);
      }
      

      google.setOnLoadCallback(drawVisualization);
    </script>
		<script src="https://labeleditor.cortext.net/svg_postmessage.js"></script>
  </head>
  <body style="font-family: Arial;border: 0 none;">
    <div id="visualization" style="width: 1200px; height: 900px;"></div>
		<div style="margin: 0.5rem;">
  <a id="svg-editor-link" style="
        color:#FFFFFF;
        text-align: center;
        padding-left: 0.5rem;
        padding-right: 0.5rem;
        padding-top: 0.25rem;
        background:#48bb78;
        border: 1px solid #2f855a;
        border-radius: 0.25rem;
        cursor: pointer;
        /*! margin: 0.5rem; */
        white-space: nowrap;
      ">
    Edit labels
  </a>
</div>

  </body>
</html>
"""


html_partcolor="""
      );
            var options = {
            title: 'MCA analysis',
            hAxis: {title: 'Dim 1'},
            vAxis: {title: 'Dim 2'},
			colorAxis: {colors: ['blue', 'red']},
            bubble: {opacity: .8, textStyle: {fontSize: 11}}
          };
      
          // Create and draw the visualization.
          var chart = new google.visualization.BubbleChart(
              document.getElementById('visualization'));
          chart.draw(data, options);
      }
      

      google.setOnLoadCallback(drawVisualization);
    </script>
		<script src="https://labeleditor.cortext.net/svg_postmessage.js"></script>
  </head>
  <body style="font-family: Arial;border: 0 none;">
    <div id="visualization" style="width: 1200px; height: 900px;"></div>
		<div style="margin: 0.5rem;">
  <a id="svg-editor-link" style="
        color:#FFFFFF;
        text-align: center;
        padding-left: 0.5rem;
        padding-right: 0.5rem;
        padding-top: 0.25rem;
        background:#48bb78;
        border: 1px solid #2f855a;
        border-radius: 0.25rem;
        cursor: pointer;
        /*! margin: 0.5rem; */
        white-space: nowrap;
      ">
    Edit labels
  </a>
</div>

  </body>
</html>
"""
print 'writing html_MCA_filename: ',html_MCA_filename
fileout=open(html_MCA_filename,'w')
fileout.write(html_part1)
fileout.write(json.dumps(final))
fileout.write(html_part2.replace('11',str(int(float(11)*float(sizefactor)*2))).replace('Dim 1' ,'Dim 1 (' + str(weight['dim 1'])+'%)').replace('Dim 2' ,'Dim 2 (' + str(weight['dim 2'])+'%)'))

dim3=True
if dim3:


	#et enfin avec le code couleur
	html_MCA_filenamecolor= os.path.join(result_path,burt_file_name.replace('.txt','color.html'))
	fileoutcolor=open(html_MCA_filenamecolor,'w')
	fileoutcolor.write(html_part1)
	legende = ['name','x','y','z','size']
	final = [legende]
	for x,y in dict_nodes.iteritems():
		try:#NB0_0 removed
			finalrow = []
			if y['z']>1:
				y['z']=1
			if y['z']<-1:
				y['z']=-1
			
			for leg in legende:
				finalrow.append(y[leg])
			final.append(finalrow)
		except:
			pass
			
	fileoutcolor.write(json.dumps(final))
	fileoutcolor.write(html_partcolor.replace('11',str(int(float(11)*float(sizefactor)*2))).replace('Dim 1' ,'Dim 1 (' + str(weight['dim 1'])+'%)').replace('Dim 2' ,'Dim 2 (' + str(weight['dim 2'])+'%)'))



	legende = ['name','x','y','groupe','size']
	final = [legende]
	legende = ['name','x','z','groupe','size']
	for x,y in dict_nodes.iteritems():
		try:#NB0_0 removed
			finalrow = []
			for leg in legende:
				finalrow.append(y[leg])
			final.append(finalrow)
		except:
			pass

	html_MCA_filename2= os.path.join(result_path,burt_file_name.replace('.txt','2.html'))
	fileout2=open(html_MCA_filename2,'w')
	fileout2.write(html_part1)
	fileout2.write(json.dumps(final))
	fileout2.write(html_part2.replace('11',str(int(float(11)*float(sizefactor)*2))).replace('Dim 1' ,'Dim 1 (' + str(weight['dim 1'])+'%)').replace('Dim 2' ,'Dim 3 (' + str(weight['dim 3'])+'%)'))


	legende = ['name','x','y','groupe','size']
	final = [legende]
	legende = ['name','y','z','groupe','size']
	for x,y in dict_nodes.iteritems():
		try:#NB0_0 removed
			print 'y',y
			finalrow = []
			for leg in legende:
				#print 'leg',leg
				finalrow.append(y[leg])
			final.append(finalrow)
		except:
			pass

	html_MCA_filename3= os.path.join(result_path,burt_file_name.replace('.txt','3.html'))
	fileout3=open(html_MCA_filename3,'w')
	fileout3.write(html_part1)
	fileout3.write(json.dumps(final))
	fileout3.write(html_part2.replace('11',str(int(float(11)*float(sizefactor)*2))).replace('Dim 2' ,'Dim 3 (' + str(weight['dim 3'])+'%)').replace('Dim 1' ,'Dim 2 (' + str(weight['dim 2'])+'%)'))

if not parameters_user.get('file_output',False):
	os.unlink(os.path.join(result_path,burt_file_name))
logging.info('Correspondance Analysis successfully terminated')
fonctions.progress(result_path0,100)


# chaine2 = """
# x=2
# print(x)
# library(FactoMineR)
# gmo <- read.table(\""""
# chaine2 = chaine2+burt_file_name
# chaine2 =chaine2+"""",header=FALSE,sep="\\t")
# res.mca <- MCA(gmo,quali.sup=bonus)
# pdf("ellipse.pdf")
# plotellipses(res.mca)
# dev.off()
# """
# 
# if len(tables_source_supp)==0:
# 	print chaine2.replace('myplot.pdf',MCA_filename).replace('ellipse.pdf',MCA_filename_ellipse).replace(',quali.sup=bonus','').replace(',cex=0.5',',cex='+str(sizefactor))
# 	r(chaine2.replace('myplot.pdf',MCA_filename).replace('ellipse.pdf',MCA_filename_ellipse).replace(',quali.sup=bonus','').replace(',cex=0.5',',cex='+str(sizefactor)))
# else:	
# 	print chaine2.replace('myplot.pdf',MCA_filename).replace('ellipse.pdf',MCA_filename_ellipse).replace(',quali.sup=bonus',',quali.sup=c('+ str(nb_col_active+1)+':'+str(nb_col_passive+nb_col_active) +')').replace(',cex=0.5',',cex='+str(sizefactor))
# 	r(chaine2.replace('myplot.pdf',MCA_filename).replace('ellipse.pdf',MCA_filename_ellipse).replace(',quali.sup=bonus',',quali.sup=c('+ str(nb_col_active+1)+':'+str(nb_col_passive+nb_col_active) +')').replace(',cex=0.5',',cex='+str(sizefactor)))
# 
# 
# #dev.copy2pdf("myplot.pdf")



#############################################################################
#############################################################################
##########################-CLI EXPORT-#######################################
#############################################################################
#############################################################################


#sys.path.append("../")
#import descriptor
#descriptor.generate(bdd_name)
#print '$$$$$$$$$$ total elapsed time: ',': ',(time.time()-starttime0), ' seconds'

# try:
# 	cli_path = "../../cli/"
# 	result_path_yaml_out=result_path
# 	(result_path_yaml_out,tail)= os.path.split(result_path)
# 	(result_path_yaml_out2,tail2)= os.path.split(result_path_yaml_out)
# 	yaml_dict={}
# 	yaml_dict['uri'] = tail2
# 	yaml_dict['description'] = 'Heterogeneous network'
# 	yaml_dict['structure'] = 'network'
# 	yaml_dict['type'] = 'json'
# 	yaml_dict['version']={}
# 	yaml_dict['version']['major']=int(tail)
# 	yaml_dict['version']['minor']=0
# 	import json
# 	cli_command = 'php ' + cli_path + "cortextcli.php dataset add "  + result_path_yaml_out
# 	cli_command = cli_command  + " '"+json.dumps(yaml_dict) + "'"
# 	print cli_command
# 	print os.system(cli_command)
# except:
# 	pass